class Barracks < Footman  
attr_accessor :gold,:food


def initialize
  @gold = 1000
  @food = 80
end

def can_train_footman?
 (food < 2 || gold < 135) ? false : true   
end

def train_footman 
  @gold -= 135
  @food -= 2
  can_train_footman? ? Footman.new : nil
end
  
  def train_peasant
    @gold -=90
    @food -=5
    peasant = Peasant.new
    can_train_peasant? ? Peasant.new : nil 
  end

  def can_train_peasant?
    (food < 5 || gold < 90) ? false : true   
  end



end

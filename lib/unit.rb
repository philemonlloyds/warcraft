class Unit 

  attr_accessor :health_points
  attr_reader   :attack_power

  def initialize(hp,ap)
    @health_points = hp
    @attack_power = ap
  end

  def damage(value)
    self.health_points -= value
  end

  def attack!(value)
    value.damage(3)
  end

end